### Java (video)
* Learning Modern Java LiveLessons (Video Training) https://rutracker.org/forum/viewtopic.php?t=5115287
* Java Fundamentals: Collections https://rutracker.org/forum/viewtopic.php?t=5189863
* Java Fundamentals: The Java Language https://rutracker.org/forum/viewtopic.php?t=5129107
* From Collections to Streams in Java 8 Using Lambda Expressions https://rutracker.org/forum/viewtopic.php?t=5080398
* Java Concurrency LiveLessons https://rutracker.org/forum/viewtopic.php?t=5057908
* Java Generics https://rutracker.org/forum/viewtopic.php?t=5058743
* Concurrent and Parallel Programming Concepts Training Video https://rutracker.org/forum/viewtopic.php?t=5145848
* Java Advanced Topics https://rutracker.org/forum/viewtopic.php?t=5374646
* LiveLessons - OCA Java SE 8 Programmer I http://rutracker.org/forum/viewtopic.php?t=5160628
* Master Practical Java 9 Development https://rutracker.org/forum/viewtopic.php?t=5483370
* Learning Java 9 Modularity http://rutracker.org/forum/viewtopic.php?t=5508399
* Functional Programming with Streams in Java 9 http://rutracker.org/forum/viewtopic.php?t=5486372

### JVM (video) 
* Understanding the Java Virtual Machine: Class Loading and Reflection https://rutracker.org/forum/viewtopic.php?t=5058406 
* Understanding the Java Virtual Machine: Security https://rutracker.org/forum/viewtopic.php?t=5050645

 
### Java Books
* Effective Java (3rd) https://rutracker.org/forum/viewtopic.php?t=5374646
* Sharan K. - Java 9 Revealed. For Early Adoption and Migration - 2017 https://rutracker.org/forum/viewtopic.php?t=5394727
 
### Spring (video)
* Learning Spring Batch https://rutracker.org/forum/viewtopic.php?t=5292230
* Architecting Web Applications with Spring https://rutracker.org/forum/viewtopic.php?t=5252563


### Docker 
* Getting Started with Docker https://rutracker.org/forum/viewtopic.php?t=5266660
* Docker for Java Developers https://rutracker.org/forum/viewtopic.php?t=5480676
* Introduction to Kubernetes using Docker https://rutracker.org/forum/viewtopic.php?t=5296243
* Docker and Containers: The Big Picture https://rutracker.org/forum/viewtopic.php?t=5145678
* Docker Deep Dive https://rutracker.org/forum/viewtopic.php?t=5505091
* Docker for Web Developers https://rutracker.org/forum/viewtopic.php?t=5206122
* Continuous Delivery Using Docker And Ansible  https://rutracker.org/forum/viewtopic.php?t=5294752
* Play by Play: Docker for Web Developers with John Papa and Dan Wahlin https://rutracker.org/forum/viewtopic.php?t=5295386


### Linux 
* LFCS: Linux Essentials https://rutracker.org/forum/viewtopic.php?t=5156022
* LFCS: Linux Operation Essentials https://rutracker.org/forum/viewtopic.php?t=5172294
* LFCS: Linux User and Group Management https://rutracker.org/forum/viewtopic.php?t=5184150
* LFCS: Linux Storage Management https://rutracker.org/forum/viewtopic.php?t=5205684
* LFCS: Linux Networking https://rutracker.org/forum/viewtopic.php?t=5456854
* LFCS: Linux Service Management https://rutracker.org/forum/viewtopic.php?t=5452220
* LFCS: Linux Virtualization Management https://rutracker.org/forum/viewtopic.php?t=5458257


### Vagrant 
* Learning Vagrant https://rutracker.org/forum/viewtopic.php?t=5299096
* Introduction to Versioning Environments With Vagrant  http://rutracker.org/forum/viewtopic.php?t=4840159

### JHipster
* JHipster: Build and Deploy Spring Boot Microservices https://rutracker.org/forum/viewtopic.php?t=5503003

### ElasticSearch
* Elasticsearch 5 and Elastic Stack - In Depth and Hands On! https://rutracker.org/forum/viewtopic.php?t=5445915
* Learning ElasticSearch 5.0 https://rutracker.org/forum/viewtopic.php?t=5411423
* Learning Path: The Road to Elasticsearch https://rutracker.org/forum/viewtopic.php?t=5412962
* Searching and Analyzing Data with Elasticsearch - Getting Started https://rutracker.org/forum/viewtopic.php?t=5427460
* ElasticSearch, LogStash, Kibana ELK #1 - Learn ElasticSearch https://rutracker.org/forum/viewtopic.php?t=5410026

### MongoDB
* The Complete Developers Guide to MongoDB  https://rutracker.org/forum/viewtopic.php?t=5400895
* MongoDB: Security http://rutracker.org/forum/viewtopic.php?t=5484484
* Learning MongoDB Training Video https://rutracker.org/forum/viewtopic.php?t=5026924
* MongoDB Administration https://rutracker.org/forum/viewtopic.php?t=5023702

### Software Architecture
* Software Architecture Fundamentals Understanding the Basics https://rutracker.org/forum/viewtopic.php?t=5167377
* Software Architecture Fundamentals Beyond the Basics https://rutracker.org/forum/viewtopic.php?t=5035887
* Software Architecture Fundamentals Soft Skills https://rutracker.org/forum/viewtopic.php?t=5035893
* Modeling for Software Architects Training Video https://rutracker.org/forum/viewtopic.php?t=5121695
* Service-Based Architectures https://rutracker.org/forum/viewtopic.php?t=5091873
* I'm a Software Architect, Now What? https://rutracker.org/forum/viewtopic.php?t=5166994


### TypeScript
[Understanding TypeScript](magnet:?xt=urn:btih:b7640fbda04d8044d0e6510a1866907ea38f0f30&dn=Udemy+-+Understanding+TypeScript&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.fastcast.nz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com)

